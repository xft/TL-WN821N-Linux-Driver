#!/bin/bash

echo "##################################################"
echo "Realtek Wi-Fi driver Auto installation script"
echo "##################################################"

# make clean
make clean
Error=$?

# compile
make -j$(nproc); Error=$?

# Check whether or not the driver compilation is done
module=`ls *.ko`
echo "##################################################"
if [ "$Error" != 0 ]; then
	echo "Compile make driver error: $Error"
	echo "Please check error Mesg"
	echo "##################################################"
	exit
else
	echo "Compile make driver ok!!"
	echo "##################################################"
fi

echo "Authentication requested [root] for remove driver:"
sudo rmmod $module
echo "Authentication requested [root] for install driver:"
sudo make install
echo "Authentication requested [root] for insert driver:"
sudo insmod $module

echo "##################################################"
echo "The Setup Script is completed !"
echo "##################################################"
